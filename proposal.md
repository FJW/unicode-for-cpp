Author: Florian Weber ⟨mail at florianjw de⟩


Modernizing C++'s syntax (Version 2)
====================================

C++ was born in an era when even the avialability of Ascii could not be taken for granted.
While we as a community have since removed support for the worst issues caused by this (trigraphs), we have failed to do anything but cut ties.
I propose to change this by moving into a slightly more modern era and allow the use of certain unicode-characters in places where they make sense.

* **How are we supposed to enter these characters?**
	* For a lot of people I envision the correct answer to be “YOU don’t, your auto-formatter will do that for you.”
	* Some people may prefer to not use them at all, which is completely compatibal with the base-proposal.
	* Some people may actually have these and other characters on their keyboard; I’m for example using a [custom layout]() that makes entering them easy.
* **But why?**
	* Code is much more often read than written. So even if there would be a minor inconvenience when writing it, the increased readability would still be worth it.
	* Several proposed alternatives are there to disambiguate the meaning of certain tokens. Yes, a compiler has rules to do them same, that is why auto-formatters
	  will work with this proposal, but humans may still be confused. Consider the infamous example `A < B, C > D`: If you consistently apply this proposal
	  The case where `A` is a template would become `A⟨B, C⟩ D`, wheras the two comparrisions would stay the same.
	* In the extreme case where the use of `operator=` is banned in favor of one of the alternatives presented below, it could prevent accidential comparrisions
	  in places where assignements were intended. (`if(a = b) {…}`)
	* Because it looks better and sends a signal that we are not stuck in the eighties.
* **Why not just use fonts with ligatures?**
	* While a nice idea, ligatures inherently lack the ability to differ based on the semantic context. They can display “<” either as a mathematical
	  operator **or** as angle-bracket, they cannot distinguish between use.
	* Ligatures are really just a bandaid to lessen the pain of not having the proper characters, when we could just allow their use.
	* There is a very high chance, that they may not be in use on other systems; while there seems to be a widespread fear that this might be the case
	  with unicode-characters as well, realistically speaking this is **much** less of a widespread issue and can furthermore be very reasonably blamed on the user/admin,
	  whereas this is not true for ligatures.
	* Some ligatures are highly language-specific. There are for example formal-verification languages that use ligatures to represent `/\` as “∧”, which is
	  a sequence that could appear in string-literals in C++ where it would have a very different meaning.
* **Is this backwards-compatible?**
	* Yes! (with the usual and practically irrelevant exception that you can use SFINAE to check whether any given syntax is valid),
	  In particular all characters that this proposal adds are invalid as parts of identifiers at this point in time.
* **How difficult is this to implement?**
	* An implementation that is complete (accepts every valid piece of code) but not sound (allows some invalid code) is extremely trivial
	  and can be performed with `sed`, see command below.
	* An implementation that is both complete and sound should also be fairly easy: It would essentially have to add an alternative token
	  that would get treated the same way as the base token, but cause an error in case of an invalid use. (That said, I’m not a compiler-developer and
	  only received limited training on that topic, so this is really more of an educated guess than anything else.)

With these out of the way, here are my initial proposals; first some unconditional alternative tokens:

* `operator!=`, `operator<=`, `operator>=`
	* “≠”, “≤”, “≥” should become alternative tokens.
* `operator==`
	* “≟” could become an alternative to again deal with the `operator=`/`operator==` problem, though I’m not fully convinced either.
* `operator->`
	* “→” should become an alternative token.
* `operator^`
	* “⊕” should become an alternative: “⊕” is by far the most common way how exclusive or is written in the literature and I consider it
	  extremely unfortunate that “^” took it’s place when it is much more commonly used to indicate exponentiation.
* `operator-`
	* „−“ should become an alternative token.
	* “-”, aka “Hypen-minus” is a weird hybrid between a minus-sign and a hyphen, whereas “−” is the proper minus-sign. The difference is in length:
	  hyphens are short, whereas a minus should have the same width as a plus, For monospace-fonts with their usual focus on programming this may not
	  be properly displayed, but when used with a proportional font, the difference may be stark.
	* The benefit of this individual addition is admittedly not huge, but neither is the const that this adds to the overall proposal.

The following alternatives are conditional, meaning that they whether they are alternative tokens depends on the way they are used:

* `operator=`
	* I consider it unfortunate that we use “=” for assignments for several reasons, the biggest one being that it forces the equality operator to be `operator==`
	  which causes bugs on a regular basis.
	* The two obvious alternatives would be “←” and “≔”. Both are very commonly used in pseudo-code and easy to understand.
	* An interessting option here would be to use “≔” for assignments to constants and “←” for everything else, which would communicate the difference in meaning
	  between two different operations:

	  ```
	  const auto x ≔ 23;
	  auto y ← 42;
	  y ← 43;
	  ```
* `operator &&`, `operator ||` and `operator!`
	* “∧”, “∨” and “¬” should become alternative tokens; for `operator||` and `operator!` this would be unconditionally,
	  for `operator&&` only if it is used as an actual operator as opposed to an
	  rvalue-reference.
	* `and`, `or` and `not` are already alternatives that may often even be preferable, but specifically for boolean formulas the operators seem less cluttered.
	  Using the words for logic regarding code-flow and the operators for calculations would give the best of both worlds:

	  ```
	  if (check_formulas and ((𝑥₀ ∧ ¬𝑥₁ ∧ ¬𝑥₂) ∧ (¬𝑥₀ ∧ 𝑥₁ ∧ 𝑥₂)) {
	  	…
	  }
	  ```
	* An semi-related and (unlike the remainder of this proposal) breaking change would be to make `and` conditional as well.
	  It seems highly unlikely that this would break any code that is not just bad, but on top intentionally confusing to prove a point.
* Non-operator “<” and “>”
	* Where not used as a binary operator, but as poor-mans angle-brackets (aka: in templates, preprocessor-includes and imports) “<” should
	  be replaced by “⟨” and “>” by “⟩”.
	* As outlined above this would disambiguate some pieces of code that are really only accessible, because of the names that the used tokens have.
* Binary `operator*`
	* Either “×” or “⋅” should become an alternative token.
	* Both are classical multiplication-signs, which the asterisk is not; using either of them would make it much more obvious where
	  things are related to multiplication and where to pointer (which under this proposal would keep using the asterisk for declaration and dereferencing.)
* `return`-statement
	* “⇒” (and possibly `=>`) would become alternatives to `return`.
	* This change would make using lambdas much nicer, who often get bloated by the return statement. Compare: `[](auto&& a, auto&& b){return a⋅b + 1;}` with
	  `[](auto&& a, auto&& b){⇒ a⋅b + 1;}`. While the parameters are still longer than they really should be, the resulting lambda is already less cluttered.
	  This addition would also open the (not proposed here) future extension of: `[](auto&& a, auto&& b) ⇒ (a⋅b + 1)` which with other proposals might make
	  `[](a,b) ⇒ (a⋅b +1)` a thing.

Example
-------

Combined this would make the following code valid:

```
#include ⟨vector⟩
#include ⟨ranges⟩

using ℤ ≔ int;
using ℕ ≔ unsigned;
using ℝ ≔ double;

auto foobar(ℕ x, ℤ y) → std::vector⟨ℝ⟩ {
	auto ret ← std::vector⟨ℝ⟩{};
	for (auto a ∈ std::views::iota(0u, x)) {
		if (a ⋅ 2 ≥ 23 or ret.size() ≠ 42) {
			const auto tmp ≔ a ⊕ y;
			ret.push_back(tmp);
		}
	}
	⇒ ret;
}
```

Possible Further Extensions
===========================

* Allowing typographic quotation-marks („“”«») for string-literals.
	* Not proposed as there is no clear consensus about what the opening and closing ones are
	  (Germany uses „foo“, whereas “foo” would be correct in english, this means that the closing german quotation-mark is actually the opening one in English.
	  Similar issues exist with «» vs »«)
	  and because it is useful to not having to quote them when wanting to print quotation-marks in user-facing output.
* allowing “∈” as alternative to `:` in foreach-statements
	* While nice, the benefit wouldn’t be large and it would prevent or at least complicate the introduction of “∈” as a new operator,
	  which appears more useful to me.
* Addition of new operators.
	* There are many that might be useful, for example “∈” and “∉” to check membership, but since that would not be a fully backwards-compatible change, I don’t propose it here.


Is this a joke?
---------------

Well, the first version of this was published on the first of April 2021, which was not an accident.
That said, I would sincerely like this to become a real thing and believe that in light of pretty much
everyone using code-formatters at this point, truly believe that this is practical.


Appendix: Unchecked Implementation
----------------------------------

```shell
sed \
	-e 's/⟨/\</' \
	-e 's/⟩/\>/' \
	-e 's/→/->/' \
	-e 's/≔/=/' \
	-e 's/≤/<=/' \
	-e 's/≥/>=/' \
	-e 's/≠/!=/' \
	-e 's/⊕/^/' \
	-e 's/∧/and/' \
	-e 's/∨/or/' \
	-e 's/¬/not/' \
	-e 's/−/-/' \
	-e 's/×/*/' \
	-e 's/⇒/return/' \
```
