#include ⟨vector⟩
#include ⟨ranges⟩

auto foobar(ℕ x, ℤ y) → std::vector⟨ℝ⟩ {
	var ret ≔ std::vector⟨ℝ⟩{};
	for (cref a ∈ std::views::iota(0u, x)) {
		if (a ⋅ 2 ≥ 23 ∨ ret.size() ≠ 42) {
			let tmp = a ⊕ y;
			ret.push_back(tmp);
		}
	}
	⇒ ret;
}
